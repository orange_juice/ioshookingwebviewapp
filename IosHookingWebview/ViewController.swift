//
//  ViewController.swift
//  IosHookingWebview
//
//  Created by Phil Goo Kang on 16/07/2017.
//  Copyright © 2017 Phil Goo Kang. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController, UIWebViewDelegate, UNUserNotificationCenterDelegate {
    
    // webview to show our site
    @IBOutlet var webView : UIWebView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // load our webpage
        webView.loadRequest(URLRequest(url: NSURL(string: "http://onoffmix.philgookang.com")! as URL))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        // get internet protocol
        let urlString = request.url?.absoluteString
        
        // check if url is larger than 9, do not want pointer error exception
        if (urlString?.characters.count)! > 9 {
            
            if urlString?.range(of: "onoffmix:") != nil{
                
                let action = getParams(url: (request.url?.absoluteString)!, key: "action") as String
                
                if action == "toast" {
                    
                    Toast(text: "토스트").show()
                    
                    return false
                } else if action == "notification" {
                    
                    let content = UNMutableNotificationContent()
                    content.title = "알림"
                    content.body = "이것은 알림 입니다."
                    
                    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
                    let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)

                    UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                    
                    return false
                }
            }
        }
        
        return true;
    }
    
    func getParams(url: String, key : String) -> String {
        if let queryItems = NSURLComponents(string: url)?.queryItems {
            for item in queryItems {
                if item.name == key {
                    return item.value!
                }
            }
        }
        return ""
    }
}

